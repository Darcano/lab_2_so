//Clase del Sistema Opertativo
class So {
  constructor(kernel, stack, heap, encabezado) {
    this.kernel = kernel;
    this.stack = stack;
    this.heap = heap;
    this.encabezado = encabezado;
  }

  crearTabla() {
    //Se elimina si existe una tabla anteriormente
    const tablaSoAntigua = document.getElementById("elementosTablaSistema");
    while (tablaSoAntigua.firstChild) {
      tablaSoAntigua.removeChild(tablaSoAntigua.firstChild);
    }

    const tabla = document.querySelector("#elementosTablaSistema");

    /*KERNEL */
    const tr = document.createElement("tr"); //Se crear una Fila Para la tabla
    tr.id = "tabla_sistema_kernel";

    const td = document.createElement("td");
    td.innerHTML = "Kernel"; //Columna Nombre
    tr.appendChild(td);

    const td2 = td.cloneNode();
    td2.innerHTML = this.kernel.valor + " " + this.kernel.unidad; //Columna Memoria
    tr.appendChild(td2);

    const td3 = td.cloneNode();
    td3.innerHTML = to_bytes(this.kernel).valor; //Columna Memoria en bytes
    tr.appendChild(td3);

    /*STACK */
    const trB = document.createElement("tr"); //Se crear una Fila Para la tabla
    trB.id = "tabla_sistema_stack";

    const tdB = document.createElement("td");
    tdB.innerHTML = "Stack"; //Columna Nombre
    trB.appendChild(tdB);

    const td2B = td.cloneNode();
    td2B.innerHTML = this.stack.valor + " " + this.stack.unidad; //Columna Memoria
    trB.appendChild(td2B);

    const td3B = td.cloneNode();
    td3B.innerHTML = to_bytes(this.stack).valor; //Columna Memoria en bytes
    trB.appendChild(td3B);

    /*HEAP */
    const trC = document.createElement("tr"); //Se crear una Fila Para la tabla
    trC.id = "tabla_sistema_kernel";

    const tdC = document.createElement("td");
    tdC.innerHTML = "Heap"; //Columna Nombre
    trC.appendChild(tdC);

    const td2C = td.cloneNode();
    td2C.innerHTML = this.heap.valor + " " + this.heap.unidad; //Columna Memoria
    trC.appendChild(td2C);

    const td3C = td.cloneNode();
    td3C.innerHTML = to_bytes(this.heap).valor; //Columna Memoria en bytes
    trC.appendChild(td3C);

    /*ENCABEZADO */
    const trD = document.createElement("tr"); //Se crear una Fila Para la tabla
    trD.id = "tabla_sistema_kernel";

    const tdD = document.createElement("td");
    tdD.innerHTML = "Encabezado"; //Columna Nombre
    trD.appendChild(tdD);

    const td2D = td.cloneNode();
    td2D.innerHTML = this.encabezado.valor + " " + this.encabezado.unidad; //Columna Memoria
    trD.appendChild(td2D);

    const td3D = td.cloneNode();
    td3D.innerHTML = to_bytes(this.encabezado).valor; //Columna Memoria en bytes
    trD.appendChild(td3D);

    tabla.appendChild(tr);
    tabla.appendChild(trB);
    tabla.appendChild(trC);
    tabla.appendChild(trD);
  }
}

//Funcion conversoras
function to_bytes(tam) {
  if (tam.unidad === "MiB") {
    return { unidad: "bytes", valor: tam.valor * 1024 * 1024 };
  } else if (tam.unidad === "KiB") {
    return { unidad: "bytes", valor: tam.valor * 1024 };
  } else if (tam.unidad === "bytes") {
    return { unidad: "bytes", valor: tam.valor };
  }
}

//Funcion conversora Decimal a Hex
function decToHex(num) {
  return num.toString(16).toUpperCase();
}

//Funcion para agregar ceros
function addCeros(hex) {
  const longCade = hex.length;
  const cerosFalt = 6 - longCade;
  let ceros = "";
  for (let index = 0; index < cerosFalt; index++) {
    ceros = ceros + "0";
  }

  return ceros + hex;
}

//Clase de proceso
class Proceso {
  constructor(pid, nombre, text, data, bss, SistemO, particion) {
    this.pid = pid;
    this.nombre = nombre;
    this.text = to_bytes(text);
    this.data = to_bytes(data);
    this.bss = to_bytes(bss);
    const tamDisco =
      this.text.valor +
      this.data.valor +
      this.bss.valor +
      to_bytes(SistemO.encabezado).valor;
    this.disco = { unidad: "bytes", valor: tamDisco };
    const memoIni = {
      unidad: "bytes",
      valor:
        this.text.valor +
        this.data.valor +
        this.bss.valor +
        to_bytes(SisO.encabezado).valor +
        to_bytes(SisO.heap).valor +
        to_bytes(SisO.stack).valor,
    };
    this.memoInicial = memoIni;
    this.particion = { data: "", bss: "", text: "", heap: "", stack: "" };
  }

  //Funcion para crear el checkBox de un proceso
  crearBoton() {
    const contenedorProcesos = document.getElementById("listaDeLosProcesos");

    if (!document.getElementById("check_lista_proceso_" + this.pid)) {
      const divBoton = document.createElement("div");
      divBoton.id = "check_lista_proceso_" + this.pid;
      divBoton.setAttribute("class", "checkbox-JASoft");

      const inputBoton = document.createElement("input");
      inputBoton.setAttribute("id", "check_" + this.pid);
      inputBoton.setAttribute("onclick", "addProceso(id)");
      inputBoton.setAttribute("type", "checkbox");
      inputBoton.setAttribute("value", "Valor");

      const labelBoton = document.createElement("label");
      labelBoton.setAttribute("for", "check_" + this.pid);
      labelBoton.innerText = "---";

      const pProceso = document.createElement("p");
      pProceso.id = "p_lista_proceso_" + this.pid;
      pProceso.innerText = this.nombre;

      divBoton.appendChild(inputBoton);
      divBoton.appendChild(labelBoton);

      contenedorProcesos.appendChild(divBoton);
      contenedorProcesos.appendChild(pProceso);
    }
  }

  //Funcion para agrear el proceso a la tabla
  agregarATablaProcesos() {
    const tr = document.createElement("tr"); //Se crear una Fila Para la tabla
    //Esa fila tiene un Id, que se compone de la palabra tabla y del pid del proceso. Ejemplo: tabla_p1
    tr.id = "tabla_" + this.pid;

    const td = document.createElement("td");
    td.innerHTML = this.pid; //Columna Item
    tr.appendChild(td);

    const td2 = td.cloneNode();
    td2.innerHTML = this.nombre; //Columna Nombre
    tr.appendChild(td2);

    const td3 = td.cloneNode();
    td3.innerHTML = this.disco.valor; //Columna Tam Disco
    tr.appendChild(td3);

    const td4 = td.cloneNode();
    td4.innerHTML = this.text.valor; //Columna Tam codigo
    tr.appendChild(td4);

    const td5 = td.cloneNode();
    td5.innerHTML = this.data.valor; //Columna Tam datos ini
    tr.appendChild(td5);

    const td6 = td.cloneNode();
    td6.innerHTML = this.bss.valor; //Columna Tam datos sin ini
    tr.appendChild(td6);

    const td7 = td.cloneNode();
    td7.innerHTML = this.memoInicial.valor; //Columna Tam inicial
    tr.appendChild(td7);

    let ramLlena = true;
    listParticiones.forEach((partition, index) => {
      if (partition.estado && index != 0) {
        ramLlena = false;
      }
    });

    document.getElementById("elementosTablaProceso").appendChild(tr);
  }

  removerATablaProcesos() {
    var idString = "#tabla_" + this.pid;
    if (document.getElementById("tabla_" + this.pid)) {
      const tr = document.querySelector(idString);
      tr.remove();
    }
  }

  agregarARam() {
    let ajuste = document.querySelector('input[name="ajuste"]:checked').value;
    for (let index = 0; index < 5; index++) {
      let boolSelec = true;
      let elementoInsertar;
      switch (index) {
        case 0:
          elementoInsertar = { Dato: this.data, nombre: "data" };
          break;
        case 1:
          elementoInsertar = { Dato: this.bss, nombre: "bss" };
          break;
        case 2:
          elementoInsertar = { Dato: this.text, nombre: "text" };
          break;
        case 3:
          elementoInsertar = { Dato: to_bytes(SisO.heap), nombre: "heap" };
          break;
        case 4:
          elementoInsertar = { Dato: to_bytes(SisO.stack), nombre: "stack" };
          break;
      }
      if (true) {
        if (ajuste === "primer") {
          listParticiones.forEach((element, index) => {
            if (index != 0) {
              if (
                element.estado == true &&
                element.tam.valor >= elementoInsertar.Dato.valor &&
                boolSelec
              ) {
                const particionSelect = crearParticionDinamica(
                  listParticiones,
                  element.id,
                  elementoInsertar.Dato.valor
                );

                this.crearElementoRam(
                  element.id,
                  particionSelect.id,
                  particionSelect.tam,
                  this.nombre + " (" + elementoInsertar.nombre + ")",
                  elementoInsertar.Dato.valor
                );

                this.particion[elementoInsertar.nombre] = particionSelect.id;
                particionSelect.estado = false;
                boolSelec = false;
              }
            }
          });
        }

        if (ajuste == "segundo") {
          let mejor_tam = ram.tamRam;
          let mejor = NaN;
          listParticiones.forEach((element, index) => {
            if (index != 0) {
              if (
                element.estado == true &&
                element.tam.valor >= elementoInsertar.Dato.valor &&
                boolSelec
              ) {
                let diferencia =
                  to_bytes(element.tam).valor -
                  to_bytes(elementoInsertar.Dato).valor;

                if (diferencia < mejor_tam) {
                  mejor_tam = diferencia;
                  mejor = element;
                }
              }
            }
          });
          const particionSelect = crearParticionDinamica(
            listParticiones,
            mejor.id,
            elementoInsertar.Dato.valor
          );

          this.crearElementoRam(
            particionSelect.id,
            particionSelect.id,
            particionSelect.tam,
            this.nombre + " (" + elementoInsertar.nombre + ")",
            elementoInsertar.Dato.valor
          );

          this.particion[elementoInsertar.nombre] = particionSelect.id;
          particionSelect.estado = false;
          boolSelec = false;
        }

        if (ajuste === "tercero") {
          let peor_tam = 0;
          let peor = NaN;
          listParticiones.forEach((element, index) => {
            if (index != 0) {
              if (
                element.estado == true &&
                element.tam.valor >= elementoInsertar.Dato.valor &&
                boolSelec
              ) {
                let diferencia =
                  to_bytes(element.tam).valor -
                  to_bytes(elementoInsertar.Dato).valor;

                if (diferencia > peor_tam) {
                  peor_tam = diferencia;
                  peor = element;
                }
              }
            }
          });
          const particionSelect = crearParticionDinamica(
            listParticiones,
            peor.id,
            elementoInsertar.Dato.valor
          );

          this.crearElementoRam(
            particionSelect.id,
            particionSelect.id,
            particionSelect.tam,
            this.nombre + "(" + elementoInsertar.nombre + ")",
            elementoInsertar.Dato.valor
          );
          this.particion[elementoInsertar.nombre] = particionSelect.id;
          particionSelect.estado = false;
          boolSelec = false;
        }
        /*******************************************************************************************************************************/
        console.log("Indice:", index);
        console.log(JSON.parse(JSON.stringify(elementoInsertar)));
        console.log(JSON.parse(JSON.stringify(this.particion)));
        console.log(JSON.parse(JSON.stringify(listParticiones)));
        /*******************************************************************************************************************************/
      }
    }
  }

  removerARam() {
    for (let index = 0; index < 5; index++) {
      let particionEliminar;
      switch (index) {
        case 0:
          particionEliminar = this.particion["data"];
          break;
        case 1:
          particionEliminar = this.particion["bss"];
          break;
        case 2:
          particionEliminar = this.particion["text"];
          break;
        case 3:
          particionEliminar = this.particion["heap"];
          break;
        case 4:
          particionEliminar = this.particion["stack"];
          break;
      }
      var idString = "#particion_memoria_" + particionEliminar;
      if (document.getElementById("particion_memoria_" + particionEliminar)) {
        const div = document.querySelector(idString);
        div.remove();

        var idStringName = "#nombre_proceso_ram_" + particionEliminar;
        const div2 = document.querySelector(idStringName);
        div2.remove();

        listParticiones.forEach((element) => {
          if (element.id == particionEliminar) {
            element.estado = true;
          }
        });
      }
    }
    this.particion = { data: "", bss: "", text: "", heap: "", stack: "" };
    for (let index = 0; index < 6; index++) {
      unirParticionesVacias();
    }
    /*******************************************************************************************************************************/

    /*******************************************************************************************************************************/
  }

  crearElementoRam(elementId, idParticion, tamParticion, nombre, tamElement) {
    const contenedor = document.querySelector(
      "#contenedor_proceso_" + elementId
    );

    const div = document.createElement("div");
    div.id = "particion_memoria_" + idParticion;
    div.setAttribute("class", "procesoRam");
    const valorPorct = (tamElement * 100) / to_bytes(tamParticion).valor;
    div.style.height = valorPorct + "%";

    const pNameProce = document.createElement("div");
    pNameProce.id = "nombre_proceso_ram_" + idParticion;
    pNameProce.setAttribute("class", "procesoNombreParticion");
    pNameProce.innerHTML = nombre;
    contenedor.appendChild(pNameProce);

    contenedor.appendChild(div);
  }

  agregarTablaSegmentos() {
    const contenedorTablas = document.getElementById("TablaDescriptor");

    const div = document.createElement("div");
    div.id = "tabla_Segmento_" + this.pid;

    const p = document.createElement("p");
    p.setAttribute("class", "tituloTabla");
    p.innerText = "Proceso " + this.nombre;

    const table = document.createElement("table");
    table.setAttribute("class", "tablaSO");

    const thead = document.createElement("thead");

    const tr = document.createElement("tr");

    const th = document.createElement("th");
    th.innerText = "Segmento";

    const th2 = th.cloneNode();
    th2.innerText = "Base";

    const th3 = th.cloneNode();
    th3.innerText = "Tamaño";

    const th4 = th.cloneNode();
    th4.innerText = "Tipo";

    const tbody = document.createElement("thead");

    for (let index = 0; index < 5; index++) {
      let segmentoDescripcion;
      switch (index) {
        case 0:
          segmentoDescripcion = { Dato: this.data, nombre: "data" };
          break;
        case 1:
          segmentoDescripcion = { Dato: this.bss, nombre: "bss" };
          break;
        case 2:
          segmentoDescripcion = { Dato: this.text, nombre: "text" };
          break;
        case 3:
          segmentoDescripcion = { Dato: to_bytes(SisO.heap), nombre: "heap" };
          break;
        case 4:
          segmentoDescripcion = { Dato: to_bytes(SisO.stack), nombre: "stack" };
          break;
      }
      const tr2 = document.createElement("tr");

      const thA = document.createElement("th");
      thA.innerText = index;

      const thA2 = thA.cloneNode();
      const particionContendor = document.getElementById(
        "particion_" + this.particion[segmentoDescripcion.nombre]
      );
      thA2.innerText = particionContendor.firstChild.firstChild.innerText;

      const thA3 = thA.cloneNode();
      thA3.innerText = segmentoDescripcion.Dato.valor;

      const thA4 = thA.cloneNode();
      thA4.innerText = segmentoDescripcion.nombre;

      tr2.appendChild(thA);
      tr2.appendChild(thA2);
      tr2.appendChild(thA3);
      tr2.appendChild(thA4);

      tbody.appendChild(tr2);
    }

    tr.appendChild(th);
    tr.appendChild(th2);
    tr.appendChild(th3);
    tr.appendChild(th4);

    thead.appendChild(tr);

    table.appendChild(thead);
    table.appendChild(tbody);

    div.appendChild(p);
    div.appendChild(table);

    contenedorTablas.appendChild(div);

    /*
     <div>
          <p class="tituloTabla">Proceso P1</p>
          <table class="tablaSO">
            <thead>
              <tr>
                <th>Segmento</th>
                <th>Base</th>
                <th>Tamaño</th>
                <th>Tipo</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    */
  }

  eliminarTablaSegmentos() {
    const eliminar = document.getElementById("tabla_Segmento_" + this.pid);
    eliminar.remove();
  }
}

//Clase de la RAM
class Ram {
  constructor(tamParti, tamRam) {
    this.tamPart = tamParti;
    this.cant = tamParti.length;
    this.tamRam = tamRam;
  }

  dibujarRam() {
    const ramDibu = document.getElementById("dibujoDeLaRam");
    //const tablaParticion = document.getElementById("elementosTablaParticion");

    //While para eliminar eliminar las particiones si existen
    while (ramDibu.firstChild) {
      ramDibu.removeChild(ramDibu.firstChild);
    }

    // //While para eliminar los hijos de la tabla de particion si existen
    // while (tablaParticion.firstChild) {
    //   tablaParticion.removeChild(tablaParticion.firstChild);
    // }

    // Se eliminan las particiones si existen
    listParticiones.splice(0, listParticiones.length);

    //For para dibujar las particiones
    let posBase = 0;
    let posLimit = 0;

    for (let index = 0; index < this.cant; index++) {
      const div = document.createElement("div");
      div.id = "particion_" + index;
      div.setAttribute("class", "particiones");
      div.style.minHeight =
        (4000 * this.tamPart[index].valor) / this.tamRam + "px";
      div.style.maxHeight =
        (4000 * this.tamPart[index].valor) / this.tamRam + "px";

      const divPos = document.createElement("div");
      divPos.setAttribute("class", "posicionRam");

      const divProceso = document.createElement("div");
      divProceso.id = "contenedor_proceso_" + index;
      divProceso.style.height = "100%";
      divProceso.setAttribute("class", "contenedorProceso");

      const divPosUp = document.createElement("div");
      divPosUp.setAttribute("class", "hexRam");

      divPosUp.innerText = "0x" + addCeros(decToHex(posBase));
      posBase = posBase + to_bytes(this.tamPart[index]).valor;

      const divPosName = document.createElement("div");
      divPosName.setAttribute("class", "procesoNombreRam");

      posLimit = posLimit + to_bytes(this.tamPart[index]).valor;
      const divPosDown = document.createElement("div");
      divPosDown.setAttribute("class", "hexRam");
      divPosDown.innerText = "0x" + addCeros(decToHex(posLimit - 1));

      divPos.appendChild(divPosUp);
      divPos.appendChild(divPosName);
      divPos.appendChild(divPosDown);

      div.appendChild(divPos);
      div.appendChild(divProceso);

      ramDibu.appendChild(div);

      let particion = new Particion(index, tamPart[index], true);
      listParticiones.push(particion);
    }

    //Dibujar la tabla de Particiones
    //listParticiones.forEach((e) => addMemTable(e));

    //Crear porceso del Sistema Operativo
    const contenedorSistema = document.querySelector("#contenedor_proceso_0");

    const divSistema = document.createElement("div");
    divSistema.id = "particion_memoria_0";
    divSistema.setAttribute("class", "procesoRam");
    const valorPorct =
      (to_bytes(SisO.kernel).valor * 100) /
      to_bytes(listParticiones[0].tam).valor;
    divSistema.style.height = valorPorct + "%";

    const pNameProce = document.createElement("div");
    pNameProce.id = "nombre_proceso_ram_0";
    pNameProce.setAttribute("class", "procesoNombreParticion");
    pNameProce.innerHTML = "Kernel";
    contenedorSistema.appendChild(pNameProce);

    contenedorSistema.appendChild(divSistema);

    listParticiones[0].estado = false;

    //Colocar la lista de procesos nuevamente para eliminar los que estaban activos
    const listaProcesosChecks = document.getElementById("listaDeLosProcesos");
    while (listaProcesosChecks.firstChild) {
      listaProcesosChecks.removeChild(listaProcesosChecks.firstChild);
    }

    //Eliminar del proceso a que particion pertenece
    listProceCreate.forEach((element) => {
      if (element.particion != "") {
        element.removerATablaProcesos();
      }
      element.crearBoton();
    });

    //Se verifica que no quede nada en la tabla
    const residuoTablaProcesos = document.getElementById(
      "elementosTablaProceso"
    );
    while (residuoTablaProcesos.firstChild) {
      residuoTablaProcesos.removeChild(residuoTablaProcesos.firstChild);
    }

    //Se actualiza la tabla de memoria
    actualizarMemoria();

    actualizarTablaLibres();
  }
}

//Clase de la particion
class Particion {
  constructor(id, tam, estado) {
    this.id = id;
    this.tam = to_bytes(tam);
    this.estado = estado;
  }
}

//Objeto del sistema operativo
let SisO = new So(
  { unidad: "MiB", valor: 1 },
  { unidad: "KiB", valor: 64 },
  { unidad: "KiB", valor: 128 },
  { unidad: "bytes", valor: 180 }
);

//Objetos de los procesos
const proNote = new Proceso(
  "p1",
  "Notepad",
  { unidad: "MiB", valor: 1 },
  { unidad: "MiB", valor: 1 },
  { unidad: "MiB", valor: 1 },
  SisO,
  ""
);
const proWord = new Proceso(
  "p2",
  "Word",
  { unidad: "MiB", valor: 1 },
  { unidad: "MiB", valor: 1 },
  { unidad: "MiB", valor: 2 },
  SisO,
  ""
);
const proExcel = new Proceso(
  "p3",
  "Excel",
  { unidad: "KiB", valor: 995 },
  { unidad: "KiB", valor: 125 },
  { unidad: "bytes", valor: 500 },
  SisO,
  ""
);
const proAuto = new Proceso(
  "p4",
  "AutoCAD",
  { unidad: "KiB", valor: 1150 },
  { unidad: "KiB", valor: 1230 },
  { unidad: "bytes", valor: 1123 },
  SisO,
  ""
);
const proCalc = new Proceso(
  "p5",
  "Calculadora",
  { unidad: "KiB", valor: 560 },
  { unidad: "KiB", valor: 256 },
  { unidad: "bytes", valor: 256 },
  SisO,
  ""
);

let listProceCreate = [proNote, proWord, proExcel, proAuto, proCalc]; //Lista de procesos creados
var listParticiones = []; //Lista de Particiones

function getProceso(pid, listaProcesos) {
  let proceso;
  listaProcesos.forEach((element) => {
    if (pid == element.pid) {
      proceso = element;
    }
  });

  return proceso;
}

const tamRam = to_bytes({ unidad: "MiB", valor: 16 }); //Tamaño de la RAM
const tamPart = [
  { unidad: "MiB", valor: 1 },
  { unidad: "MiB", valor: 15 },
]; //Tamaño de las particiones

const bytes_tamPart = [];

tamPart.forEach((e) => bytes_tamPart.push(to_bytes(e)));

//Objeto de la RAM
let ram = new Ram(bytes_tamPart, tamRam.valor);

ram.dibujarRam();

// function addMemTable(particion) {
//   const tr = document.createElement("tr"); //Se crear una Fila Para la tabla
//   //Esa fila tiene un Id, que se compone de la palabra tabla y del pid del proceso. Ejemplo: tabla_p1
//   "tablamem_" + particion.id;

//   const td = document.createElement("td");
//   td.innerHTML = particion.tam.valor / (1024 * 1024); //Columna Item
//   tr.appendChild(td);

//   const td2 = td.cloneNode();
//   td2.innerHTML = particion.tam.valor / 1024; //Columna Nombre
//   tr.appendChild(td2);

//   const td3 = td.cloneNode();
//   td3.innerHTML = particion.tam.valor; //Columna Tam Disco
//   tr.appendChild(td3);

//   //document.getElementById("elementosTablaParticion").appendChild(tr);
// }

SisO.crearTabla();

//Se crean los proceso de acuerdo a la lista
listProceCreate.forEach((element) => {
  element.crearBoton();
});

//Funcion para agregar un proceso a ejecucion
window.addProceso = function addProceso(id) {
  var isChecked = document.getElementById(id).checked;

  const pid = id.split("_")[1];
  const proceso = getProceso(pid, listProceCreate);

  if (isChecked) {
    proceso.agregarARam();
    proceso.agregarATablaProcesos();
    proceso.agregarTablaSegmentos();
  } else {
    proceso.removerARam();
    proceso.removerATablaProcesos();
    proceso.eliminarTablaSegmentos();
  }

  actualizarMemoria();
  actualizarTablaLibres();

  // const tablaParticion = document.getElementById("elementosTablaParticion");

  // //While para eliminar los hijos de la tabla de particion si existen
  // while (tablaParticion.firstChild) {
  //   tablaParticion.removeChild(tablaParticion.firstChild);
  // }

  // listParticiones.map((partition) => {
  //   addMemTable(partition);
  // });
};

//Funcion para crear una particion dinamica respecto a un proceso
function crearParticionDinamica(
  listaParticionesActual,
  idParticion,
  tamProceso
) {
  let tamParticionesNuevas = [];
  let listaParticionesNueva = [];
  let cont = 0;
  let tamTotalParticion;
  let diferencia;

  //Creamos los tamaños nuevos para las particiones
  listaParticionesActual.map((element, index) => {
    //if (index != 0) {
    if (index != idParticion) {
      tamParticionesNuevas.push(element.tam);
      listaParticionesNueva.push(
        new Particion(element.id + cont, element.tam, element.estado)
      );
    } else {
      tamTotalParticion = to_bytes(element.tam);
      diferencia = tamTotalParticion.valor - tamProceso;
      tamParticionesNuevas.push({ unidad: "bytes", valor: tamProceso });
      listaParticionesNueva.push(
        new Particion(
          element.id + cont,
          { unidad: "bytes", valor: tamProceso },
          element.estado
        )
      );
      if (diferencia > 0) {
        cont++;
        tamParticionesNuevas.push({ unidad: "bytes", valor: diferencia });
        listaParticionesNueva.push(
          new Particion(
            element.id + cont,
            { unidad: "bytes", valor: diferencia },
            element.estado
          )
        );
      }
    }
    //}
  });

  //Creamos una nueva ram con estas particiones
  ram = new Ram(tamParticionesNuevas, ram.tamRam);

  //Obtenemos el contenedor que queremos reemplazar
  const contenedorProcesoOld = document.getElementById(
    "particion_" + idParticion
  );

  //Actualizamos los elementos siguientes
  for (
    let index = listaParticionesActual.length - 1;
    index > idParticion;
    index--
  ) {
    cambiarParticionProceso(index, index + cont);
    const contenedorActualDelFor = document.getElementById(
      "particion_" + index
    );

    contenedorActualDelFor.id = "particion_" + (index + cont);

    const subcontenedorActualDelFor = document.getElementById(
      "contenedor_proceso_" + index
    );
    subcontenedorActualDelFor.id = "contenedor_proceso_" + (index + cont);

    const nombreProcesoDelFor = document.getElementById(
      "nombre_proceso_ram_" + index
    );
    if (nombreProcesoDelFor) {
      nombreProcesoDelFor.id = "nombre_proceso_ram_" + (index + cont);
    }

    const ocupacionProcesoDelFor = document.getElementById(
      "particion_memoria_" + index
    );
    if (ocupacionProcesoDelFor) {
      ocupacionProcesoDelFor.id = "particion_memoria_" + (index + cont);
    }
  }

  //Calculo matematico para saber los px que va a ocupar las particiones
  let porcentajeParticionProceso;
  let porcentajeParticionSobrante;
  let tamNewParticion;
  let tamNewSobrante;
  if (cont == 0) {
    tamNewParticion = contenedorProcesoOld.style.maxHeight.slice(0, -2);
  } else {
    let tamOldParticion = parseFloat(
      contenedorProcesoOld.style.maxHeight.slice(0, -2)
    );
    porcentajeParticionProceso = (tamProceso * 100) / tamTotalParticion.valor;
    porcentajeParticionSobrante = 100 - porcentajeParticionProceso;

    tamNewParticion = tamOldParticion * (porcentajeParticionProceso / 100);
    tamNewSobrante = tamOldParticion * (porcentajeParticionSobrante / 100);
  }

  const div = document.createElement("div");
  div.id = "particion_" + idParticion;
  div.setAttribute("class", "particiones");
  div.style.minHeight = tamNewParticion + "px";
  div.style.maxHeight = tamNewParticion + "px";

  const divPos = document.createElement("div");
  divPos.setAttribute("class", "posicionRam");

  const divProceso = document.createElement("div");
  divProceso.id = "contenedor_proceso_" + idParticion;
  divProceso.style.height = "100%";
  divProceso.setAttribute("class", "contenedorProceso");

  const divPosUp = document.createElement("div");
  divPosUp.setAttribute("class", "hexRam");

  divPosUp.innerText =
    contenedorProcesoOld.childNodes[0].childNodes[0].innerText;

  const divPosName = document.createElement("div");
  divPosName.setAttribute("class", "procesoNombreRam");

  let posLimit =
    parseInt(contenedorProcesoOld.childNodes[0].childNodes[0].innerText, 16) +
    tamProceso -
    1;
  const divPosDown = document.createElement("div");
  divPosDown.setAttribute("class", "hexRam");
  divPosDown.innerText = "0x" + addCeros(decToHex(posLimit - 1));

  divPos.appendChild(divPosUp);
  divPos.appendChild(divPosName);
  divPos.appendChild(divPosDown);

  div.appendChild(divPos);
  div.appendChild(divProceso);

  //Inserto la primer particion antes de la particion que vamos a eliminar
  document
    .getElementById("dibujoDeLaRam")
    .insertBefore(div, contenedorProcesoOld);

  if (cont != 0) {
    const div2 = document.createElement("div");
    div2.id = "particion_" + (idParticion + cont);
    div2.setAttribute("class", "particiones");
    div2.style.minHeight = tamNewSobrante + "px";
    div2.style.maxHeight = tamNewSobrante + "px";

    const div2Pos = document.createElement("div");
    div2Pos.setAttribute("class", "posicionRam");

    const div2Proceso = document.createElement("div");
    div2Proceso.id = "contenedor_proceso_" + (idParticion + cont);
    div2Proceso.style.height = "100%";
    div2Proceso.setAttribute("class", "contenedorProceso");

    const div2PosUp = document.createElement("div");
    div2PosUp.setAttribute("class", "hexRam");

    div2PosUp.innerText = "0x" + addCeros(decToHex(posLimit));

    const div2PosName = document.createElement("div");
    div2PosName.setAttribute("class", "procesoNombreRam");

    const div2PosDown = document.createElement("div");
    div2PosDown.setAttribute("class", "hexRam");
    div2PosDown.innerText =
      contenedorProcesoOld.childNodes[0].childNodes[2].innerText;

    div2Pos.appendChild(div2PosUp);
    div2Pos.appendChild(div2PosName);
    div2Pos.appendChild(div2PosDown);

    div2.appendChild(div2Pos);
    div2.appendChild(div2Proceso);

    //Inserto la segunda particion antes de la particion que vamos a eliminar
    document
      .getElementById("dibujoDeLaRam")
      .insertBefore(div2, contenedorProcesoOld);
  }

  listParticiones = listaParticionesNueva;

  contenedorProcesoOld.remove();

  return listParticiones[idParticion];
}

function unirParticionesVacias() {
  let cont = 0;
  let tamParticionesNuevas = [];
  let listaParticionesNueva = [];
  let ajutadorId = 0;
  listParticiones.map((partitiom, index) => {
    if (partitiom.estado) {
      cont++;
    } else {
      cont = 0;
    }
    if (cont == 2) {
      const indiceUnificado = index - 1;

      const particionViejaUno = document.getElementById("particion_" + index);
      const particionViejaDos = document.getElementById(
        "particion_" + indiceUnificado
      );

      const tamViejaUno = parseFloat(
        particionViejaUno.style.maxHeight.slice(0, -2)
      );
      const tamViejaDos = parseFloat(
        particionViejaDos.style.maxHeight.slice(0, -2)
      );

      const tamHexArriba =
        particionViejaDos.childNodes[0].childNodes[0].innerText;
      const tamHexAbajo =
        particionViejaUno.childNodes[0].childNodes[2].innerText;

      const tamViejoTotal = tamViejaDos + tamViejaUno;

      //Creamos la particion unificada
      const divUnificada = document.createElement("div");
      divUnificada.id = "particion_" + indiceUnificado;
      divUnificada.setAttribute("class", "particiones");
      divUnificada.style.minHeight = tamViejoTotal + "px";
      divUnificada.style.maxHeight = tamViejoTotal + "px";

      const divUnificadaPos = document.createElement("div");
      divUnificadaPos.setAttribute("class", "posicionRam");

      const divUnificadaProceso = document.createElement("div");
      divUnificadaProceso.id = "contenedor_proceso_" + indiceUnificado;
      divUnificadaProceso.style.height = "100%";
      divUnificadaProceso.setAttribute("class", "contenedorProceso");

      const divUnificadaPosUp = document.createElement("div");
      divUnificadaPosUp.setAttribute("class", "hexRam");

      divUnificadaPosUp.innerText = tamHexArriba;

      const divUnificadaPosName = document.createElement("div");
      divUnificadaPosName.setAttribute("class", "procesoNombreRam");

      const divUnificadaPosDown = document.createElement("div");
      divUnificadaPosDown.setAttribute("class", "hexRam");
      divUnificadaPosDown.innerText = tamHexAbajo;

      divUnificadaPos.appendChild(divUnificadaPosUp);
      divUnificadaPos.appendChild(divUnificadaPosName);
      divUnificadaPos.appendChild(divUnificadaPosDown);

      divUnificada.appendChild(divUnificadaPos);
      divUnificada.appendChild(divUnificadaProceso);

      //Verificamos si existe un particion despues de las que estan vacias y seguidas
      const partitionBefore = document.getElementById(
        "particion_" + (index + 1)
      );

      particionViejaDos.remove();
      particionViejaUno.remove();

      const dibujoRam = document.getElementById("dibujoDeLaRam");
      if (partitionBefore) {
        dibujoRam.insertBefore(divUnificada, partitionBefore);
      } else {
        dibujoRam.appendChild(divUnificada);
      }

      //Actualizamos los elementos siguientes
      for (
        let iterador = index + 1;
        iterador < listParticiones.length;
        iterador++
      ) {
        cambiarParticionProceso(iterador, iterador - 1);
        const contenedorActualDelFor = document.getElementById(
          "particion_" + iterador
        );

        contenedorActualDelFor.id = "particion_" + (iterador - 1);

        const subcontenedorActualDelFor = document.getElementById(
          "contenedor_proceso_" + iterador
        );
        subcontenedorActualDelFor.id = "contenedor_proceso_" + (iterador - 1);

        const nombreProcesoDelFor = document.getElementById(
          "nombre_proceso_ram_" + iterador
        );
        if (nombreProcesoDelFor) {
          nombreProcesoDelFor.id = "nombre_proceso_ram_" + (iterador - 1);
        }

        const ocupacionProcesoDelFor = document.getElementById(
          "particion_memoria_" + iterador
        );
        if (ocupacionProcesoDelFor) {
          ocupacionProcesoDelFor.id = "particion_memoria_" + (iterador - 1);
        }
      }
      const tamAux = partitiom.tam.valor + listParticiones[index - 1].tam.valor;

      //Agregamos la conjunta a las listas
      tamParticionesNuevas[index - 1].valor = tamAux;
      listaParticionesNueva[index - 1].tam.valor = tamAux;
      ajutadorId = 1;
    } else {
      //Agregamos la particion normalmente a las listas
      tamParticionesNuevas.push(partitiom.tam);
      listaParticionesNueva.push(
        new Particion(
          partitiom.id - ajutadorId,
          partitiom.tam,
          partitiom.estado
        )
      );
    }
  });
  ram = new Ram(tamParticionesNuevas, ram.tamRam);
  listParticiones = listaParticionesNueva;
}

function cambiarParticionProceso(oldParticion, NewParticion) {
  listProceCreate.map((partition) => {
    for (let index = 0; index < 5; index++) {
      let elementoSeleccionar;
      switch (index) {
        case 0:
          elementoSeleccionar = { Dato: this.data, nombre: "data" };
          break;
        case 1:
          elementoSeleccionar = { Dato: this.bss, nombre: "bss" };
          break;
        case 2:
          elementoSeleccionar = { Dato: this.text, nombre: "text" };
          break;
        case 3:
          elementoSeleccionar = { Dato: to_bytes(SisO.heap), nombre: "heap" };
          break;
        case 4:
          elementoSeleccionar = { Dato: to_bytes(SisO.stack), nombre: "stack" };
          break;
      }
      if (partition.particion[elementoSeleccionar.nombre] == oldParticion) {
        partition.particion[elementoSeleccionar.nombre] = NewParticion;
      }
    }
  });
}

/*Funcion para hacer el Form para agregar un proceso con el boton*/
window.crearProcesoBoton = function crearProcesoBoton() {
  const botonCrear = document.querySelector("#boton_Crear_Proceso");
  botonCrear.remove();

  const div = document.querySelector("#botones_Modificar_Proceso");

  const divForm = document.createElement("div");
  divForm.id = "Form_Crear_Proceso";
  divForm.setAttribute("class", "formProceso");

  const select = document.createElement("select");

  const inputPid = document.createElement("input");
  inputPid.id = "input_proceso_pid_agregar";
  inputPid.type = "text";
  inputPid.placeholder = "Pid";

  const inputNombre = document.createElement("input");
  inputNombre.id = "input_proceso_nombre_agregar";
  inputNombre.type = "text";
  inputNombre.placeholder = "Nombre";

  const inputText = document.createElement("input");
  inputText.id = "input_proceso_text_agregar";
  inputText.type = "text";
  inputText.placeholder = "Text";

  const selectText = select.cloneNode();
  selectText.id = "select_proceso_text_agregar";
  selectText.name = "Unidad";
  const optionB = document.createElement("option");
  optionB.value = "MiB";
  optionB.innerText = "MiB";
  const option2B = document.createElement("option");
  option2B.value = "KiB";
  option2B.innerText = "KiB";
  const option3B = document.createElement("option");
  option3B.value = "bytes";
  option3B.innerText = "bytes";
  selectText.appendChild(optionB);
  selectText.appendChild(option2B);
  selectText.appendChild(option3B);

  const inputData = document.createElement("input");
  inputData.id = "input_proceso_data_agregar";
  inputData.type = "text";
  inputData.placeholder = "Data";

  const selectData = select.cloneNode();
  selectData.id = "select_proceso_data_agregar";
  selectData.name = "Unidad";
  const optionC = document.createElement("option");
  optionC.value = "MiB";
  optionC.innerText = "MiB";
  const option2C = document.createElement("option");
  option2C.value = "KiB";
  option2C.innerText = "KiB";
  const option3C = document.createElement("option");
  option3C.value = "bytes";
  option3C.innerText = "bytes";
  selectData.appendChild(optionC);
  selectData.appendChild(option2C);
  selectData.appendChild(option3C);

  const inputBss = document.createElement("input");
  inputBss.id = "input_proceso_bss_agregar";
  inputBss.type = "text";
  inputBss.placeholder = "Bss";

  const selectBss = select.cloneNode();
  selectBss.id = "select_proceso_bss_agregar";
  selectBss.name = "Unidad";
  const optionD = document.createElement("option");
  optionD.value = "MiB";
  optionD.innerText = "MiB";
  const option2D = document.createElement("option");
  option2D.value = "KiB";
  option2D.innerText = "KiB";
  const option3D = document.createElement("option");
  option3D.value = "bytes";
  option3D.innerText = "bytes";
  selectBss.appendChild(optionD);
  selectBss.appendChild(option2D);
  selectBss.appendChild(option3D);

  const buttonSave = document.createElement("button");
  buttonSave.innerHTML = "Guardar";
  buttonSave.setAttribute("onclick", "guardarProcesoBoton()");

  divForm.appendChild(inputPid);
  divForm.appendChild(inputNombre);
  divForm.appendChild(inputText);
  divForm.appendChild(selectText);
  divForm.appendChild(inputData);
  divForm.appendChild(selectData);
  divForm.appendChild(inputBss);
  divForm.appendChild(selectBss);
  divForm.appendChild(buttonSave);

  div.appendChild(divForm);
};

/*Funcion para hacer el Form para eliminar un proceso con el boton*/
window.eliminarProcesoBoton = function eliminarProcesoBoton() {
  const botonEliminar = document.querySelector("#boton_Eliminar_Proceso");
  botonEliminar.remove();

  const div = document.querySelector("#botones_Modificar_Proceso");

  const divForm = document.createElement("div");
  divForm.id = "Form_Eliminar_Proceso";
  divForm.setAttribute("class", "formProceso");

  const inputPid = document.createElement("input");
  inputPid.id = "input_proceso_pid_eliminar";
  inputPid.type = "text";
  inputPid.placeholder = "Pid";

  const buttonDelete = document.createElement("button");
  buttonDelete.innerHTML = "Eliminar";
  buttonDelete.setAttribute("onclick", "removerProcesoBoton()");

  divForm.appendChild(inputPid);
  divForm.appendChild(buttonDelete);

  div.appendChild(divForm);
};

/*Funcion para guarda el proceso con el boton */
window.guardarProcesoBoton = function guardarProcesoBoton() {
  const pid = document.querySelector("#input_proceso_pid_agregar").value;
  const nombre = document.querySelector("#input_proceso_nombre_agregar").value;
  const text = document.querySelector("#input_proceso_text_agregar").value;
  const textSelect = document.querySelector(
    "#select_proceso_text_agregar"
  ).value;
  const data = document.querySelector("#input_proceso_data_agregar").value;
  const dataSelect = document.querySelector(
    "#select_proceso_data_agregar"
  ).value;
  const bss = document.querySelector("#input_proceso_bss_agregar").value;
  const bssSelect = document.querySelector("#select_proceso_bss_agregar").value;

  if (
    pid &&
    nombre &&
    text &&
    textSelect &&
    data &&
    dataSelect &&
    bss &&
    bssSelect &&
    !getProceso(pid, listProceCreate)
  ) {
    const procesoNew = new Proceso(
      pid,
      nombre,
      { unidad: textSelect, valor: parseInt(text) },
      { unidad: dataSelect, valor: parseInt(data) },
      { unidad: bssSelect, valor: parseInt(bss) },
      SisO,
      ""
    );

    listProceCreate.push(procesoNew);

    //Se crean los proceso de acuerdo a la lista
    listProceCreate.forEach((element) => {
      element.crearBoton();
    });

    const formCrear = document.querySelector("#Form_Crear_Proceso");
    formCrear.remove();

    const div = document.querySelector("#botones_Modificar_Proceso");

    const botonCrear = document.createElement("button");
    botonCrear.id = "boton_Crear_Proceso";
    botonCrear.innerHTML = "Crear";
    botonCrear.setAttribute("onclick", "crearProcesoBoton()");

    div.appendChild(botonCrear);
  }
};

/*Funcion para eliminar el proceso con el boton */
window.removerProcesoBoton = function removerProcesoBoton() {
  const pid = document.querySelector("#input_proceso_pid_eliminar").value;

  if (pid) {
    const proceso = getProceso(pid, listProceCreate);
    removeProcessTable(proceso);
    removeProcessRam(proceso);

    const checkProceso = document.querySelector(
      "#check_lista_proceso_" + proceso.pid
    );
    checkProceso.remove();

    const pProceso = document.querySelector("#p_lista_proceso_" + proceso.pid);
    pProceso.remove();

    listProceCreate = listProceCreate.filter((item) => item.pid !== pid);

    const formCrear = document.querySelector("#Form_Eliminar_Proceso");
    formCrear.remove();

    const div = document.querySelector("#botones_Modificar_Proceso");

    const botonEliminar = document.createElement("button");
    botonEliminar.id = "boton_Eliminar_Proceso";
    botonEliminar.innerHTML = "Eliminar";
    botonEliminar.setAttribute("onclick", "eliminarProcesoBoton()");

    div.appendChild(botonEliminar);
  }
};

window.modParticionBoton = function modParticionBoton() {
  const button = document.querySelector("#boton_modificar_particion");
  button.remove();

  const div = document.querySelector("#div_modificar_particiones");

  const divForm = document.createElement("div");
  divForm.id = "Form_Modificar_Particion";
  divForm.setAttribute("class", "formProceso");

  const inputTam = document.createElement("input");
  inputTam.id = "input_particion_tamano_modificar";
  inputTam.type = "text";
  inputTam.placeholder = "Tamaño";

  const select = document.createElement("select");
  select.id = "select_particion_tamano_modificar";
  select.name = "Unidad";
  const optionB = document.createElement("option");
  optionB.value = "MiB";
  optionB.innerText = "MiB";
  const option2B = document.createElement("option");
  option2B.value = "KiB";
  option2B.innerText = "KiB";
  select.appendChild(optionB);
  select.appendChild(option2B);

  const buttonDelete = document.createElement("button");
  buttonDelete.innerHTML = "Modificar";
  buttonDelete.style.gridColumn = "1 / 3";
  buttonDelete.setAttribute("onclick", "modParticionDibujo()");

  divForm.appendChild(inputTam);
  divForm.appendChild(select);
  divForm.appendChild(buttonDelete);

  div.appendChild(divForm);
};

window.modParticionDibujo = function modParticionDibujo() {
  const div = document.querySelector("#div_modificar_particiones");

  const tam = document.querySelector("#input_particion_tamano_modificar").value;
  const unidadTam = document.querySelector(
    "#select_particion_tamano_modificar"
  ).value;

  const tamPart = { unidad: unidadTam, valor: parseInt(tam) };

  const mensajeError = (p) => {
    if (!tam) {
      p.innerText = "Se debe agregar un tamaño";
    } else if (!(to_bytes(ram.tamRam).valor % to_bytes(tamPart).valor == 0)) {
      p.innerText =
        "El tamaño de la particion no debe dejar residuos en la Ram";
    } else {
      p.innerText =
        "El tamaño de la particion debe ser igual o mayor al Kernel";
    }
  };

  if (
    tam &&
    unidadTam &&
    to_bytes(ram.tamRam).valor % to_bytes(tamPart).valor == 0 &&
    to_bytes(tamPart).valor >= to_bytes(SisO.kernel).valor
  ) {
    ram = new Ram(tamPart, tamRam);
    dibujarRam(ram);
    const form = document.querySelector("#Form_Modificar_Particion");
    form.remove();

    const botonEliminar = document.createElement("button");
    botonEliminar.id = "boton_modificar_particion";
    botonEliminar.innerHTML = "Modificar particiones";
    botonEliminar.setAttribute("onclick", "modParticionBoton()");

    div.appendChild(botonEliminar);
  } else {
    const form = document.querySelector("#Form_Modificar_Particion");

    if (document.getElementById("p_particion_error_modificar")) {
      const pError = document.querySelector("#p_particion_error_modificar");
      mensajeError(pError);
      form.appendChild(pError);
    } else {
      const pError = document.createElement("p");
      pError.id = "p_particion_error_modificar";
      pError.style.gridColumn = "1 / 3";
      mensajeError(pError);
      form.appendChild(pError);
    }
  }
};

function actualizarMemoria() {
  const tablaOld = document.getElementById("elementosTablaMemoria");
  while (tablaOld.firstChild) {
    tablaOld.removeChild(tablaOld.firstChild);
  }
  const tablaMemoria = document.querySelector("#elementosTablaMemoria");

  let total = ram.tamRam;

  let usada = 0;
  let libre = 0;
  listParticiones.forEach((part) => {
    if (part.estado == false) {
      usada = usada + part.tam.valor;
    }
  });
  libre = total - usada;

  const tr = document.createElement("tr");

  const tdUsada = document.createElement("td");
  tdUsada.innerHTML = usada + " bytes";

  const tdLibre = document.createElement("td");
  tdLibre.innerHTML = libre + " bytes";

  const tdTotal = document.createElement("td");
  tdTotal.innerHTML = total + " bytes";

  tr.appendChild(tdUsada);
  tr.appendChild(tdLibre);
  tr.appendChild(tdTotal);

  tablaMemoria.appendChild(tr);

  const tr2 = document.createElement("tr");

  const tdUsada2 = document.createElement("td");
  tdUsada2.innerHTML = parseInt((usada * 100) / total) + " %";

  const tdLibre2 = document.createElement("td");
  tdLibre2.innerHTML = parseInt((libre * 100) / total) + " %";

  const tdTotal2 = document.createElement("td");
  tdTotal2.innerHTML = "100 %";

  tr2.appendChild(tdUsada2);
  tr2.appendChild(tdLibre2);
  tr2.appendChild(tdTotal2);

  tablaMemoria.appendChild(tr2);
}

function actualizarTablaLibres() {
  const tabla = document.getElementById("TablaSegmentosLibres");
  if (tabla.hasChildNodes) {
    while (tabla.firstChild) {
      tabla.removeChild(tabla.firstChild);
    }
  }

  listParticiones.forEach((element, index) => {
    if (element.estado == true) {
      const base = document.getElementById("particion_" + index).firstChild
        .firstChild.innerText;
      const tr = document.createElement("tr"); //Se crear una Fila Para la tabla

      const td = document.createElement("td");
      td.innerHTML = base; //Columna Nombre
      tr.appendChild(td);

      const td2 = td.cloneNode();
      td2.innerHTML = element.tam.valor; //Columna Memoria
      tr.appendChild(td2);

      tabla.appendChild(tr);
    }
  });
}
